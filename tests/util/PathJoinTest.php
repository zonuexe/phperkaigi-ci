<?php

namespace StormMeta\util;

/**
 * TestCase for {@see StormMeta\util\path_join()}
 *
 * @author USAMI Kenta <tadsan@zonu.me>
 * @copyright 2019 USAMI Kenta
 * @license https://www.mozilla.org/en-US/MPL/2.0/ MPL-2.0
 */
final class PathJoinTest extends \StormMeta\TestCase
{
    /**
     * @dataProvider dataProviderForTest
     */
    public function test(string $expected, array $args)
    {
        $this->assertSame($expected, path_join(...$args));
    }

    public function dataProviderForTest()
    {
        return [
            [
                '/etc/password',
                ['/etc', 'password'],
            ],
            [
                '/foo/bar',
                ['foo', 'bar'],
            ],
            [
                '/foo/bar',
                ['/foo/', 'bar'],
            ],
            [
                '/foo/bar',
                ['foo/', 'bar'],
            ],
        ];
    }
}
