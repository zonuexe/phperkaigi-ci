# PHPerKaigi 2019 ゆるふわCI入門 よくばりセット

Utility for PhpStorm's meta file.

## これは何か

このリポジトリ内の `Makefile` とか `tools` ディレクトリをコピーすれば簡単に各種QAツール等が導入できます。

それぞれどのような手順で何を実行できるかは `.gitlab-ci.yml` を見ればある程度わかると思います。

## make

### `make` / `make composer`

Composer自体と、アプリケーションが依存するパッケージをセットアップします。

### `make tools`

QAツール一式をダウンロード・セットアップします。

### `make analyse`

QAツール一式を使ってコードを解析します。

### `make setup-doc`

APIドキュメント生成に必要なツールをダウンロードします。

### `make doc` / `make phpdoc`

APIドキュメントを生成します。

## Copyright

**StormMeta** is [free software], this package is licensed under [Mozilla Public License Version 2.0][mpl-2.0].

> StormMeta - Utility for PhpStorm's meta file
>
> (C) Copyright  2019 USAMI Kenta <tadsan@zonu.me>
>
> This Source Code Form is subject to the terms of the Mozilla Public
> License, v. 2.0. If a copy of the MPL was not distributed with this
> file, You can obtain one at https://mozilla.org/MPL/2.0/ .

[`phpstorm_meta_stub.php`][phpstorm_meta_stub] is derived from [JetBrains/phpstorm-stubs: `meta/.phpstorm.meta.php`][orig_phpstorm_meta] licensed under [Apache License Version 2.0][apache-2.0].

[apache-2.0]: https://www.apache.org/licenses/LICENSE-2.0
[free software]: https://www.gnu.org/philosophy/free-sw.html
[ide-advanced-metadata]: https://www.jetbrains.com/help/phpstorm/ide-advanced-metadata.html
[mpl-2.0]: https://www.mozilla.org/en-US/MPL/2.0/
[orig_phpstorm_meta]: https://github.com/JetBrains/phpstorm-stubs/blob/f70f79c/meta/.phpstorm.meta.php
[phpstorm_meta_stub]: phpstorm_meta_stub.php
