<?php

/**
 * Utility functions for StormMeta
 *
 * @author USAMI Kenta <tadsan@zonu.me>
 * @copyright 2019 USAMI Kenta
 * @license https://www.mozilla.org/en-US/MPL/2.0/ MPL-2.0
 */
namespace StormMeta\util
{
    function path_join(string ...$parts): string
    {
        return \array_reduce($parts, function ($carry, $item) {
            return $carry . '/' .  trim($item, '/\\');
        }, '');
    }
}
